#!/usr/bin/env bash

case $1 in
  histogram)
    awk -f initialize.awk | awk -f histogram.awk
    ;;
  histogram-normalized)
    awk -f initialize.awk | awk -f histogram-normalized.awk
    ;;
  *)
    echo "Usage:"
    echo "$0 histogram"
    echo "$0 histogram-normalized"
    exit 0
esac