function multiple(symbol, time) {
  output = ""
  for (x=0; x<time; x++) {
    output=output symbol; 
  }
  return output
}

BEGIN {
  rate = 10
}

/^[0-9]+$/ {
  arr[int($1 / rate)*rate]++
}

END {
  # set sort order for arrays
  PROCINFO["sorted_in"] = "@ind_num_asc"
  for (x in arr) {
    printf ("%3d - %3d:   %3d  %s\n", x, x+(rate-1), arr[x], multiple("*", arr[x]))
  }
}