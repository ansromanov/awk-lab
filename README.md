# AWK howework by Andrey Romanov

## Prerequisites
This script works with awk, but gawk (GNU awk) realization is preffered, because it has definite associative arrays sorting mechanism.

## Run tests
1. Run histogram awk script
```shell
$ ./script.sh histogram
``` 
1. Run normalized histogram awk script
```shell
$ ./script.sh histogram-normalized
``` 