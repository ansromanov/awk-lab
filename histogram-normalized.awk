# Taken from official documentation https://www.gnu.org/software/gawk/manual/html_node/Round-Function.html
# round.awk --- do normal rounding
function round(x,   ival, aval, fraction)
{
   ival = int(x)    # integer part, int() truncates

   # see if fractional part
   if (ival == x)   # no fraction
      return ival   # ensure no decimals

   if (x < 0) {
      aval = -x     # absolute value
      ival = int(aval)
      fraction = aval - ival
      if (fraction >= .5)
         return int(x) - 1   # -2.5 --> -3
      else
         return int(x)       # -2.3 --> -2
   } else {
      fraction = x - ival
      if (fraction >= .5)
         return ival + 1
      else
         return ival
   }
}

function multiple(symbol, time) {
  output = ""
  for (x=0; x<time; x++) {
    output=output symbol; 
  }
  return output
}

BEGIN {
  rate = 10
}

/^[0-9]+$/ {
  arr[int($1 / rate)*rate]++
  count++
}

END {
  # set sort order for arrays
  PROCINFO["sorted_in"] = "@ind_num_asc"
  for (x in arr) {
    portion = arr[x] / count * 100
    printf ("%3d - %3d:   %3.1f%% %s\n", x, x+(rate-1), portion, multiple("*", round(portion)))
  }
}